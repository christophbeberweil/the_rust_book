
enum List {
    Cons(i32, Box<List>),
    Nil,
}

use std::ops::Deref;

use crate::List::{Cons, Nil};


// own boxt-like implementation
struct MyBox<T>(T);

// similar to Box::new()
impl <T> MyBox<T>{
    fn new(x: T) -> MyBox<T> {
        MyBox(x)
    }
}

impl<T> Deref for MyBox<T>{
    type Target = T; // see ch 19

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}


fn main() {
    // trivial example. 5 is stored on the
    // heap, its pointer on the stack

    let b = Box::new(5);
    println!("b = {}", b);

    // boxes allow for recursive types, for instance in
    // cons lists (lisp) (https://doc.rust-lang.org/book/ch15-01-box.html)
    

    
    let _list = Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil))))));
    

    // Deref trait

    let x = 5;
    let y = &x;

    assert_eq!(5, x);
    assert_eq!(5, *y); // * follows the reference to the value

    let x = 5;
    let y = Box::new(x);

    assert_eq!(5, x);
    assert_eq!(5, *y); // dereferencing

    let x = 5;
    let y = MyBox::new(5);
    
    assert_eq!(5, x);
    assert_eq!(5, *y); // dereferencing


}   

