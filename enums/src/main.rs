#[derive(Debug)]
enum IpAddrKind {
    V4(u8, u8, u8, u8),
    V6(String),
}


// enum variants can hold different data
enum Message {
    Quit, 
    Move {x: i32, y:i32},
    Write(String),
    ChangeColor(i32, i32, i32),
}

impl Message{
    fn call(&self){
        // handle enums depending on their "type" and payload
        match self{
            Message::Quit => println!("Quit"),
            Message::Move { x, y } => println!("Move {}, {}", x, y),
            Message::ChangeColor(r, g, b) => println!("New color is {}.{}.{}", r, g, b),
            Message::Write(s) => println!("MESSAGE: {}", s)
        }
    }
}

fn plus_one(x: Option<i32>) -> Option<i32>{
    match x{
        None => None,
        Some(i) => Some(i +1),
    }
}

fn main() {
    let four = IpAddrKind::V4(127,0,0,1);
    let six = IpAddrKind::V6(String::from("::1"));

    route(four);
    route(six);


    // using the match consruct to react to different types of messages differently

    let w = Message::Write(String::from("hello"));
    w.call();

    let q = Message::Quit;
    q.call();
    
    let m = Message::Move { x: 5, y: 64 };
    m.call();

    let c = Message::ChangeColor(255, 0, 0);
    c.call();


    // Option<T> Enum
    // can be used to represent nullable values

    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);

    println!("{:?}", five);
    println!("{:?}", six);
    println!("{:?}", none);


    // catch-all or catch-remaining with match, other can be renamed
    let dice_roll = 9;
    println!("🎲: {}", dice_roll);
    match dice_roll{
        3 => println!("You get a fancy hat"),
        7 => println!("You loose a fancy hat"),
        other => println!("you move {} steps", other),
    }

    // placeholder, does not use value and does not raise a warning for an unused value
    let dice_roll = 5;
    println!("🎲: {}", dice_roll);
    match dice_roll{
        3 => println!("You get a fancy hat"),
        7 => println!("You loose a fancy hat"),
        _ => (), // do nothing
    }

    


}


fn route(ip_kind: IpAddrKind){
    println!("{:?}", ip_kind);
}