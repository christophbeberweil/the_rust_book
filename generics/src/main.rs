use std::fmt::Display;


// define custom traits (methods that need to be implemented)
pub trait Summary{
    fn summarize(&self) -> String;
}

// provide a default implementation
pub trait EmojiRepresentation{
    fn erepr(&self) -> String{
        String::from("❓")
    }
}

pub struct NewsArticle{
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

 impl Summary for NewsArticle {
     fn summarize(&self) -> String {
         format!{"{} by {}", self.headline, self.author}
     }
 }

 // use the default impementation
 impl EmojiRepresentation for NewsArticle{}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}


// define the necessary functions
impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!{"{}: {}", self.username, self.content}
    }
}
// overwrite the default implementation
impl EmojiRepresentation for Tweet{
    fn erepr(&self) -> String {
        String::from("🐦")
    }
}




struct Pair<T> {
    x: T,
    y: T,
}


// generic "constructor"
impl<T> Pair<T>{
    fn new(x: T, y: T) -> Self{
        Self{x, y}
    }
}


// trait bounds
// this method is only implemented for T that satisfy the given traits
impl <T: Display + PartialOrd> Pair<T>{
    fn cmp_display(&self){
        if self.x >= self.y{
            println!("The largest member is x = {}", self.x);
        } else {
            println!("The largest member is y = {}", self.y);
        }
    }
}

fn main() {

    // find the largest element of a list
    let number_list = vec![25, 74, 12, 642];
    let float_list = vec![52.15, 631.23, 613.12, 71.42, 924.321];

    let int_result = largest(&number_list);
    let float_result = largest(&float_list);
    
    println!("Largest {}, {}", int_result, float_result);


    // implement traits on structs

    let tweet = Tweet{
        username: String::from("@olaf"),
        content: String::from("I found this awesome thing..."),
        reply: false,
        retweet: false,
    };


    let article = NewsArticle{
        author: String::from("Olaf Olafsen"),
        content: String::from("A very long and complicated text..."),
        headline: String::from("My Take on Things"),
        location: String::from("In the wild internet"),
    };


    // print a summary using a generic function
    // the trait ensures that summary is available
    print_summary(&tweet);
    print_summary(&article);


    // summarie anything that is summarizable 
    print_simple_summary(&return_summarizable());


    // trait bounds
    let int_pair = Pair{
        x:1, y:2
    };

    int_pair.cmp_display();

    let string_pair = Pair{
        x: String::from("lol"),
        y: String::from("rofl"),
    };
    string_pair.cmp_display();

    let failing_pair = Pair{
        x: &tweet,
        y: &tweet,
    };

    // tweet is not comparable, hence this would fail
    //failing_pair.cmp_display();


}

// normal version
fn find_largest_number(list: &[i32]) -> &i32{
    let mut largest = &list[0];

    for element in list{
        if element > largest{
            largest = element;
        }
    }

    &largest
}

// generic version
// we need to compare two elements of the same type.
// therefore, we need T to implement PartialOrd
fn largest<T>(list: &[T]) -> &T
where T: PartialOrd
{   

    let mut largest = &list[0];

    for item in list{
        if item > largest{
            largest = item;
        }
    }
    largest
}

// generic function, requiring a two traits
fn print_summary<T>(element: &T)
where T: Summary + EmojiRepresentation{
    println!("{} {}",element.erepr(), element.summarize());
}

// generic function requiring one trait
fn print_simple_summary<T>(element: &T)
where T: Summary{
    println!("> {}", element.summarize());
}


// function returning something that implements a trait
// this function must always return the same type, so always a Tweet or always a News article
fn return_summarizable() ->impl Summary{
    Tweet{
        username: String::from("horse_ebooks"),
        content: String::from("Test"),
        reply: false,
        retweet: false,
    }

}