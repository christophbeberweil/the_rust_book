pub fn add_two(input: usize) -> usize{
    add(input, 2)
}

fn add(left: usize, right: usize) -> usize {
    left + right
}

// unit tests, local to the code they are testing
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add_function() {
        let result = add(2, 2);
        assert_eq!(result, 4);

        
    }

    #[test]
    #[should_panic]
    fn exploration(){
        assert_eq!(add(1,5), 5);
    }
}
