// one integration test, tests the public api of a module

// this is compiled to an extra crate,
// so we need to bring the library into scope
use adder;

#[test]
fn it_adds_two(){
    assert_eq!(4, adder::add_two(2))
}