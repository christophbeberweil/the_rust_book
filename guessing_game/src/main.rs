use std::io;
use std::cmp::Ordering;
use rand::Rng;




fn get_random_number(min: u32, max:u32) -> u32{
    let secret_number = rand::thread_rng().gen_range(min..=max);
    return secret_number
}


fn main() {

    const MAX_AMOUNT_OF_GUESSES: u32 = 5;

    println!("Guess the number!");

    let secret_number = get_random_number(1, 20);

    let mut used_guesses = 0;

    loop {
        used_guesses += 1;
        let mut guess = String::new();

        io::stdin().read_line(&mut guess).expect("Failed to read line");

        let guess: u32 = match guess.trim().parse(){
            Ok(num) => num,
            Err(_) => continue,
        };
    
    
    
        match guess.cmp(&secret_number){
            Ordering::Less => println!("> Too small"),
            Ordering::Greater => println!("> Too big!"),
            Ordering::Equal => {
                println!("> You win :)");
                break;
            },
        }
        if used_guesses >= MAX_AMOUNT_OF_GUESSES{
            println!("> You lost");
            break;
        }
    
    }

}