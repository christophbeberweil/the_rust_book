use std::{fs::{File, self}, io::{ErrorKind, Read, self}};

fn main() {
    println!("Error handling");

    //demo_panic();
    //demo_panic_in_library();
    //demo_open_nonexistent_file();
    //demo_open_nonexistent_file_with_closure();
    //demo_open_nonexistent_file_with_unwrap_expect();
   
   
    //let username_result = demo_propagating_errors();
    let username_result = demo_propagating_errors_question_operator();

    let username = match username_result {
        Ok(v) => v,
        Err(e) => panic!("could not read username: {:?}", e),
    };

    println!("the username is: {}", username);

    match last_char_of_first_line("asasfasf\nasdaea\nasdf"){
        Some(v) => println!("Last char is {}", v),
        None => println!("Could not find last char!"),
    }
    
}

fn demo_panic(){
    println!("This will cause a panic");
    panic!("Here is a problem!");
}

fn demo_panic_in_library(){
    let v = vec![1, 2, 3];
    
    v[99]; // this panics
    // to view the backtrace, run `RUST_BACKTRACE=1 cargo run`
}

fn demo_open_nonexistent_file(){
    // handling different errors with match
    let greeting_file_result = File::open("hello.txt");
    
    let greeting_file = match greeting_file_result {
        Ok(file) => file,
        Err(error) => match error.kind(){
            ErrorKind::NotFound => match File::create("hello.txt"){
                Ok(fc)=> fc,
                Err(e) => panic!("Problem creatin the file: {:?}",e),
            },
            other_error => panic!("Problem opening the file {:?}", other_error)
        }
    };
    println!("{:?}", greeting_file);


}

fn demo_open_nonexistent_file_with_closure(){
    // handling errors with closures
    // clean up huge nested match expressions
    let greeting_file = File::open("hello.txt").unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create("hello.txt").unwrap_or_else(|error|{
                panic!("Problem creating the file: {:?}", error);
            })
        } else {
            panic!{"Problem opening the file: {:?}", error};
        }
    });
}

fn demo_open_nonexistent_file_with_unwrap_expect(){
    // panics if there is no file
    //let greeting_file = File::open("hello.txt").unwrap();

    // choose a custom error message, implicitly unwraps:
    let greeting_file = File::open("hello.txt").expect("The file is missing!");

    // expect should be used
}

fn demo_propagating_errors() -> Result<String, io::Error>{
    // return the error in form of Result

    let username_file_result = File::open("hello.txt");

    let mut username_file = match username_file_result {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut username = String::new();

    match username_file.read_to_string(&mut username) {
        Ok(_) => Ok(username),
        Err(e) => Err(e),
    }
}
fn demo_propagating_errors_question_operator() -> Result<String, io::Error>{
    // use ? to automatically return and transform the error 

    /*
    let mut username_file = File::open("hello.txt")?;
    
    let mut username = String::new();

    username_file.read_to_string(&mut username)?;
    Ok(username)
    */

    // even shorter version with chained ?:
    /*
    let mut username = String::new();

    File::open("hello.txt")?.read_to_string(&mut username)?;

    Ok(username)
    */

    // actually, in real life, one would use the function from the standard lib instead:
    // which is then an one liner...
    fs::read_to_string("hello.txt")
}

fn last_char_of_first_line(text: &str) -> Option<char>{
    text.lines().next()?.chars().last()
}