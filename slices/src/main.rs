fn main() {
    println!("Hello, world!");

    let s = String::from("Hello World");
    let first_word = first_word(&s);
    println!("{}", first_word);
    let _s2 = s;
    // this does not work, because the reference is tied to
    // s1, which gave up ownership 
    //println!("{}", first_word);


    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3];
    assert_eq!(slice, &[2, 3])
}

// &str can receive references to strings and slices of strings
fn first_word(s: &str) -> &str{
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate(){
        if item == b' '{
            return &s[0..i];
        }
    }

    &s[..]

}
