fn main() {

    // create a new, empty string
    let mut s1 = String::new();

    // string literal (str)
    let data = "initial contents";
    // convert to String
    let s2 = data.to_string();

    // alternative versions
    let s2 = "initial_contents".to_string();
    let s2 = String::from("initial_contents");

    // utf8 :)
    let hello = String::from("السلام عليكم");
    let hello = String::from("Dobrý den");
    let hello = String::from("Hello");
    let hello = String::from("שָׁלוֹם");
    let hello = String::from("नमस्ते");
    let hello = String::from("こんにちは");
    let hello = String::from("안녕하세요");
    let hello = String::from("你好");
    let hello = String::from("Olá");
    let hello = String::from("Здравствуйте");
    let hello = String::from("Hola");

    // manipulating strings

    let mut s = String::from("foo");
    
    let append_me = "bar";
    s.push_str(append_me);

    println!("{} was appended", append_me);
    println!("The string is now {}", s);

    // add a single character
    let mut text = String::from("lo");
    text.push('l');
    println!("{}", text);

    // concatenation

    let s1 = String::from("Hello, ");
    let s2 = String::from("World");
    let s3 = s1 + &s2; // invalidates s1, but s2 ist still valid
    // re-using ownership of s1 is more efficient than copying here.
    println!("{}; s2 is {}", s3, s2);

    let s1  = String::from("tic");
    let s2  = String::from("tac");
    let s3  = String::from("toe");

    let s = format!("{s1}-{s2}-{s3}");
    println!("{s}");

    // indexing into strings

    // strings cannot be indexed.
    // string is a wrapper over a Vec<u8>
    // string length will not always be the same as the amount of characters in a string

    // BUT, indexing is possible with ranges

    let hello = "Здравствуйте";

    println!("{}", &hello[0..4]);
    // println!("{}", &hello[0..1]); // panics...

    // iterating
    println!("Iterate using chars:");
    for c in hello.chars(){
        println!("{c}");
    }
    println!("Iterate using bytes:");
    for b in hello.bytes(){
        println!("{b}");
    }

}
