#[derive(Debug)]
struct Rectangle{
    width: u32,
    height: u32
}


// methods (belongs to struct)
impl Rectangle{
    fn area(&self) -> u32{
        self.width * self.height
    }
    fn enlarge(&mut self){
        self.width = self.width + 10;
        self.height = self.height + 10;
    }
    fn shrink(&mut self){
        self.width = self.width - 10;
        self.height = self.height - 10;
    }
    // with multiple parameters
    fn can_hold(&self, other: &Rectangle) ->bool{
        self.width >= other.width && self.height >= other.height
    }
}

// getters (special methods)
impl Rectangle{
    // getter for width, enable readonly access to private propertes (chapter 7)
    fn width(&self) -> u32{
        self.width
    }
}

// constructors
impl Rectangle{
    fn square(size: u32) -> Self{
        Self { width: size, height: size }
    }
}

#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn largner_can_hold_smaller(){
        let larger = Rectangle{
            height: 8,
            width: 8,
        };
        let smaller = Rectangle{
            height: 5,
            width: 5,
        };
        assert!(larger.can_hold(&smaller));
        assert!(!smaller.can_hold(&larger));
    }

    #[test]
    fn rect_can_hold_itself(){
        let rect = Rectangle{
            height: 5,
            width: 7,
        };
        // more informative error message
        assert!(
            rect.can_hold(&rect),
            "Rectangles are supposed to be contained in themselves, which was not the case for {:?}",
            rect,
        )
    }


}

fn main() {



    let mut rect = Rectangle{
        width: 30,
        height: 50,
    };
    println!("The area of {:?} is {} square pixels!", rect, rect.area());

    rect.enlarge();

    println!("The area of {:?} is {} square pixels!", rect, rect.area());
    rect.shrink();
    rect.shrink();
    println!("The area of {:?} is {} square pixels!", rect, rect.area());
    
    // read property and getter
    println!("width: {}, width(): {}", rect.width, rect.width());


    let rect_s = Rectangle{
        width: 10,
        height: 10,
    };

    let rect_m = Rectangle{
        width: 20, height:20,
    };
    let rect_l = Rectangle{
        width: 30, height: 30,
    };

    println!("m can hold s {}",rect_m.can_hold(&rect_s));
    println!("s can hold l {}",rect_s.can_hold(&rect_l));
    println!("l can hold m {}",rect_l.can_hold(&rect_m));

    let square  = Rectangle::square(25);
    println!("square can hold m {}",square.can_hold(&rect_m));

    


}
