use std::{time::Duration, thread};

#[derive(Debug, PartialEq, Copy, Clone)]
enum ShirtColor{
    Red,
    Blue
}

struct Inventory{
    shirts: Vec<ShirtColor>,
}

impl Inventory {
    fn giveaway(&self, user_prefrence: Option<ShirtColor>) -> ShirtColor{
        // define a closure that is called in an error case
        // that provides a default solution by calling a function
        user_prefrence.unwrap_or_else(|| self.most_stocked())
    }

    fn most_stocked(&self) -> ShirtColor{
        let mut num_red = 0;
        let mut num_blue = 0;

        for color in &self.shirts{
            match color{
                ShirtColor::Red => num_red += 1,
                ShirtColor::Blue => num_blue += 1,
            }
        }
        if num_red > num_blue{
            ShirtColor::Red
        } else {
            ShirtColor::Blue
        }
    }
}

fn main() {
    let store = Inventory{
        shirts: vec![
            ShirtColor::Blue,
            ShirtColor::Red,
            ShirtColor::Blue
        ],
    };

    let user_pref1 = Some(ShirtColor::Red);
    let giveaway1 = store.giveaway(user_pref1);
    println!(
        "The user with preference {:?} gets {:?}",
        user_pref1, giveaway1
    );

    let user_pref2 = None;
    let giveaway2 = store.giveaway(user_pref2);
    
    println!(
        "The user with preference {:?} gets {:?}",
        user_pref2, giveaway2
    );

    // closures can be stored in variables

    let expensive_closure = |num: u64| -> u64{
        println!("Calculating slowly...");

        for i in 1..num{
            thread::sleep(Duration::from_millis(1));
            print!("{} ", i);
        }
        println!("Result {}", num);
        num
    };

    expensive_closure(5);

    // the type can be inferred, because closures are only valid in a small scope
    // the following implementations are equivalent
    fn add_one_v1 (x: u32) -> u32 {x+1};
    let add_one_v2 = |x: u32| -> u32 {x + 1};
    let add_one_v3 = |x| {x + 1}; // type can be inferred, because closure is invoked below
    let add_one_v4 = |x| x + 1;

    add_one_v1(3);
    add_one_v2(2);
    add_one_v3(5);
    add_one_v4(9);

    let example_closure = |x| x;
    let s = example_closure(String::from("hello"));
    // does not work, because previos line assigned deferred the type to String
    // let n = example_closure(5);

    // references and ownership

    // borrow immutably, borrow mutably, taking ownership

    let list = vec![1, 2, 3];
    println!("Before defining closure: {:?}", list);

    let only_borrows = || println!("From Closure {:?}", list);

    println!("Before calling closure: {:?}", list);
    only_borrows();
    println!("After calling closure: {:?}", list);

    println!("---------------");
    // borrow mutably
    let mut list2 = vec![1, 2, 3];
    println!("Before defining closure: {:?}", list);

    let mut borrows_mutably = || list2.push(7);
    // references to list2 are forbidden here, because of the mutable borrow
    //println!("Before calling closure: {:?}", list2);


    borrows_mutably();

    println!("After calling closure: {:?}", list2);

    println!("---------------");
    // taking ownership

    let mut list3 = vec![1, 2, 3];
    println!("Before defining closure: {:?}", list3);

    thread::spawn(move || {
        println!("From thread: {:?}", list3);
        list3.push(63);
        println!("From thread: {:?}", list3);
    }).join().unwrap();


}
