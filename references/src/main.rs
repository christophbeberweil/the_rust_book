fn calculate_length(s: &String) ->usize{
    s.len()
}

fn change(s: &mut String){
    s.push_str(", world");
}

fn main() {
    let s1 = String::from("hello");
    let len = calculate_length(&s1);// send a reference(borrowing), it does not transfer ownership
    println!("The length of {} is {}", s1, len); // s1 and len are still valid


    // references can be changed when mutable

    let mut s2 = String::from("hello");

    change(&mut s2);

    println!("Updated: {}", s2);

    // there can only ever be one mutable reference at the same time

    let mut s3 = String::from("hello3");

    {
        let r1 = &mut s3;
        change(r1);
        println!("{}", r1);
    }
    let r2 = &mut s3;
    change(r2);
    println!("{}", r2);

    // actually this works, because references go 
    //out of scope when they are used for the last time
    let r3 = &mut s3;
    change(r3);
    println!("{}", r3);

    let r4 = &mut s3;
    change(r4);
    println!("{}", r4);





}
