fn main() {
    
    // creates a reference (&str)
    let s = "Hello";

    // create a string
    let mut st = String::from("hello");

    st.push_str(" hihi");
    println!("{} {}", s, st);


    // primitives make copies of each other, y is a copy of x

    let mut x = 5;
    let mut y = x;

    x -= 1;
    y += 1;

    println!("x: {}, y: {}", x, y);

    // strings (heap) are not copied, but moved

    let s1 = String::from("hello");
    let s2 = s1; // s1 goes out of scope
    println!("{}, world", s2); // s1 is not available

    // strings can be cloned, but this is not efficient and duplicates the data

    let s1 = String::from("hello");
    let s2 = s1.clone(); // s1 goes out of scope
    println!("{} {}, world", s1, s2); // s1 is not available



}    
