
//struct holding a reference with a lifetime annotation
struct ImportantExcerpt<'a> {
    part: &'a str,
}

fn main() {
    println!("Lifetimes...");

    let string1 = String::from("abcd");
    let string2 = "xyz";

    let result = longest(string1.as_str(), string2);
    
    println!("The longest string is {}", result);



    // use a lifetime annotation in a struct
    // this works, because the struct and the str have the same lifetime 
    // and 
    let novel = String::from("Call me Ishmael. Some years ago...");
    let first_sentence = novel.split('.').next().expect("Could not find a '.'");
    let i = ImportantExcerpt{
        part: first_sentence,
    };



}


// the lifetime annotation says that the lifetime of x and y are linked
// this is necessary, because it is unclear at runtime which
// reference will be returned
fn longest<'a>(x: &'a str, y: &'a str) ->&'a str{
    if x.len() > y.len(){
        x 
    } else {
        y
    }
}

// does not work, because this would return a reference to a
// local variable that will go out of scope
//fn some_string<'a>(x: & str, y: &str) -> &'a str{
//    let result = String::from("really long string");
//    result.as_str()
//}