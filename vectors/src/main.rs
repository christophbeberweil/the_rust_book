fn main() {

    // declare a vector
    let v1: Vec<i32> = Vec::new();
    println!("{:?}", v1);

    let v2 = vec![1, 2, 3];
    println!("{:?}", v2);

    //add elements to a vector

    let mut v3: Vec<i32> = Vec::new(); 

    v3.push(5);
    v3.push(6);
    v3.push(7);
    v3.push(8);
    println!("{:?}", v3);

    let last_element = v3.pop();
    match last_element{
        Some(value) => println!("last element was {}", value),
        None => println!("there was no last element (vector was empty)")
    }
    println!("{:?}", v3);


    // read elements from a vector

    let third: &i32 = &v3[2]; // crashes if integer does not exist
    println!("The third element is {}", third);

    let ninth : Option<&i32> = v3.get(9); // able to match if element does not exist
    
    match ninth{
        Some(value) => println!("The third element is {}", value),
        None => println!("The ninth element does not exist.")
    }

    // after obtaining values from a vector, it may not be changed if the reference is still valid
    
    //v3.push(8); // change
    //println!("The third element is {}", third); //reference from before change

    
    // looping

    let lv = vec![5,2, 1];
    for i in &lv{
        println!("{i}");
    }

    // looping and changing the vector

    let mut mvec = vec![5, 6, 7];
    println!("Before: {:?}", mvec);

    for i in &mut mvec{
        *i += 10; // * is the dereference operator(turns a pointer into a value)
    }
    println!("After: {:?}", mvec);


    // storing multiple types in a vector
    // with an enum!
    #[derive(Debug)]
    enum SpreadsheetCell {
        Int(i32),
        Float(f64),
        Text(String),
    }

    let row = vec![
        SpreadsheetCell::Int(4),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.124),
    ];

    println!("multiple types: {:?}", row);


    
}
