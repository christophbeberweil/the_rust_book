#[derive(Debug)]
struct User {
    active: bool,
    username: String,
    email: String,
    sign_in_count: u64,
}

// structs can also store reference (&str) values, but then they
// must be denoted with a lifetime (Chapter 10)

fn build_user(email: String, username: String) -> User {
    User{
        active: true,
        username,
        email, // same as email: email,
        sign_in_count: 0
    }
}

fn print_user(user: &User){
    // this works because of the derive(Debug) attribute of the struct
    println!("{:?}", user);
    //println!("{:#?}", user);

}

fn main() {


    let mut user1 = build_user(
        String::from("asd@asd.de"),
        String::from("asd")
    );

    user1.sign_in_count = 2;

    print_user(&user1);

    // struct update syntax, copies all other fields
    let user2 = User{
        email: String::from("another@exampe.com"),
        ..user1
    };// user 1 is not valid beyond this point because heap
    // datatypes (String username) have been copied.
    print_user(&user2);

    let user3 = User{
        email: String::from("another@exampe.com"),
        username: String::from("test"),
        ..user2
    };

    // this works, because only stack variables (bool,int) are copied:
    print_user(&user2);
    print_user(&user3);

    //
    //tuple structs
    //

    struct Color(i32, i32, i32);
    struct Point(i32, i32, i32);
    let _black = Color(0,0,0);
    let _current_location = Point(51,135,741);

    //
    // unit like structs
    // without any fields
    //

    struct AlwaysEqual;
    let _subject = AlwaysEqual;

}
