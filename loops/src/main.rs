
fn increment(number: i32)-> i32{
    number + 1
}


fn main() {

    // loop (forever or until broken)

    let mut counter = 0;

    let result = loop{
        //counter += 1;
        counter = increment(counter);
        if counter == 10{
            break counter *2;
        }
    };

    // loop with inner loop and breaking with label

    println!("Counter: {counter}, the result is {result}");
    
    let mut count: i32 = 0;
    'counting_up: loop{
        println!("count = {count}");
        let mut remaining: u32 = 10;
        loop {
            println!("remaining = {remaining}");
            if remaining == 9 {
                break;
            }
            if count == 2{
                break 'counting_up; // break 'counting_up label
            }
            remaining -= 1;
        }
        count += 1;
    }
    println!("End count = {count}");

    // while loop


    let mut number = 3;

    while number != 0{
        println!("{number}");
        number -= 1;
    }
    println!("Liftoff!");

    // for loop

    let a = [10, 20, 30, 40, 50];
    for element in a{
        println!("The value is {element}");
    }

    // for loop with range
    for number in 1..4 {
        println!("{number}")
    }

    // reverse for loop

    for number in (1..4).rev(){
        println!("{number}");
    }



}
