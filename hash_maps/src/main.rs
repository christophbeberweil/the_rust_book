use std::{collections::HashMap, sync::WaitTimeoutResult};

fn main() {
    println!("Hash Maps");

    // creating hash maps
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 22);
    println!("{:?}", scores);

    // accessing values
    let team_name = String::from("Blue");
    let score = scores.get(&team_name).copied().unwrap_or(0);

    println!("{}", score);

    // iterating

    for (key, value) in &scores{
        println!("{key}: {value}");
    }


    // ownership
    // heap values will be owned by the hash map

    let field_name = String::from("Favourite color");
    let field_value = String::from("Blue");

    let mut map = HashMap::new();

    map.insert(field_name, field_value);


    //println!("{field_name}"); // error
    //println!("{field_value}"); // error

    // references can be put in a hashmap, but the lifetime has to be considererd
    
    // updates

    let mut scores = HashMap::new();

    // this overwrites
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Blue"), 20);

    scores.insert(String::from("Red"), 20);
    // leave alone or insert
    scores.entry(String::from("Orange")).or_insert(50);

    println!("{:?}", scores);

    // updating based on previous value
    let text = "hello world wonderful world";
    let mut map = HashMap::new();

    for word in text.split_whitespace(){
        let count = map.entry(word).or_insert(0);
        *count += 1; // dereference
    }
    println!{"{:?}", map};




}
