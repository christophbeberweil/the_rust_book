use std::{env, process};

use minigrep::Config;

fn main() {
    // handle arghttps://namelix.com/appument parsing error gracefully
    let config = Config::build(env::args()).unwrap_or_else(|err| {
        //print to stderr
        eprintln!("Problem parsing arguments: {err}");
        process::exit(1);
    });

    /*
    run(config).unwrap_or_else(|err|{
        println!("Problem performing run: {err}");
        process::exit(1);
    });
    */
    // if let only looks at the error, unwrap_or_else would
    // emphasize a return value, but there is none in this case
    if let Err(e) = minigrep::run(config) {
        // print to stderr
        eprintln!("Problem performing run: {e}");
        process::exit(1);
    }
}
