use std::{env, error::Error, fs};

pub struct Config {
    pub query: String,
    pub file_path: String,
    pub ignore_case: bool,
}

impl Config {
    // takes a mutable string iterator
    pub fn build(mut args: impl Iterator<Item = String>) -> Result<Config, &'static str> {
        // first argument is the filename of the executable
        args.next();

        let query = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a query string"),
        };

        let file_path = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a file path"),
        };

        let ignore_case = env::var("IGNORE_CASE").is_ok();

        Ok(Config {
            query,
            file_path,
            ignore_case,
        })
    }
}

#[derive(Debug)]
struct Match<'a> {
    text: &'a str,
    line_number: usize,
}

impl<'a> Match<'a> {
    fn display(&self) -> String {
        format!("{}: {}", self.line_number, self.text)
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.file_path)?;

    let matched_lines = if config.ignore_case {
        search_case_insensitive(&config.query, &contents)
    } else {
        search(&config.query, &contents)
    };

    //print the matches
    for m in matched_lines {
        println! {"{}", m.display()}
    }

    Ok(())
}

fn search<'a>(query: &str, contents: &'a str) -> Vec<Match<'a>> {
    /*
    Iterate through each line of the contents.
    Check whether the line contains our query string.
    If it does, add it to the list of values we’re returning.
    If it doesn’t, do nothing.
    Return the list of results that match.
    */

    // with .enumerate() we get a tuple of the position and the value instead of just the value
    contents
        .lines()
        .enumerate()
        .filter(|(_, line)| line.contains(query))
        .map(|(index, line)| Match {
            text: line,
            line_number: index + 1,
        })
        .collect()
}

fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<Match<'a>> {
    let query = query.to_lowercase();

    contents
        .lines()
        .enumerate()
        .filter(|(_, line)| line.to_lowercase().contains(&query))
        .map(|(index, line)| Match {
            text: line,
            line_number: index + 1,
        })
        .collect()
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive,
pick three.
Duct...
        ";

        let expected = vec![Match {
            line_number: 2,
            text: "safe, fast, productive,",
        }];
        let actual = search(query, contents);

        assert_eq!(expected.len(), actual.len());

        assert_eq!(actual.len(), 1);

        for i in 0..expected.len() {
            assert_eq!(expected[i].text, actual[i].text);
            assert_eq!(expected[i].line_number, actual[i].line_number);
        }
    }

    #[test]
    fn case_insensitive() {
        let query = "rust";
        let contents = "\
Rust:
safe, fast, productive,
pick three.
        ";
        let expected = vec![Match {
            line_number: 1,
            text: "Rust:",
        }];
        let actual = search_case_insensitive(query, contents);

        assert!(
            expected.len() == actual.len(),
            "We expect to find {} results, but found {}",
            expected.len(),
            actual.len()
        );

        assert!(
            actual.len() == 1,
            "We expect to find one result, found: {}",
            actual.len()
        );

        for i in 0..expected.len() {
            assert_eq!(expected[i].text, actual[i].text);
            assert_eq!(expected[i].line_number, actual[i].line_number);
        }
    }
}
